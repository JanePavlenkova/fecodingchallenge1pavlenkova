import React from 'react';


function Modal ({handleModal}) {

     return(
         <div className="modal" onClick={handleModal}>
             <div className="modalContent">
                 <h3>Title</h3>

             </div>
         </div>)
     
}

export default Modal;