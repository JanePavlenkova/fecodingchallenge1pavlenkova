import React from 'react';

function Search({changeFunc, searchText}) {
    return (
        <input onChange={changeFunc} 
        value={searchText} 
        type="search"
        placeholder="Search by name"/>
    )
}

export default Search;