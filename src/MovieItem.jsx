import React from 'react';

function MovieItem ({movie, modalFunc}) {
    return (
        
        <div className="movieItem" onClick={modalFunc}>
            <h3 className="movieTitle">Title: {movie.Title}</h3>
            <p>Year: {movie.Year}</p>
            <p style={{textAlign: "center"}}>
                <img src={movie.Poster} alt="Movie Poster" className="moviePoster"/>
            </p>
            
        </div>
        

    )

}

export default MovieItem;