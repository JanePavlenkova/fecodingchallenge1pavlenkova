import React from 'react';
import { render } from 'react-dom';
import App from './App';
import './styles.css';

function renderApp() {
  render(<App/>, document.getElementById('root'));
}

renderApp();
