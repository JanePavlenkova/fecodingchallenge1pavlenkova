import React from 'react';
import MovieItem from './MovieItem';

class MovieList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          movieList: props.movieList,
        };
      }

      render() {
          const { movieList } = this.state;
          return (
              <>
              {movieList.map((item) => {
                  return (
                    <MovieItem movie={item} key={item.imdbID} modalFunc={this.props.modalFunc}/>
                  )
              })}
              </>
          )
      }
}
export default MovieList;