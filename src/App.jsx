import React from 'react';
import Search from './Search';
import MovieList from './MovieList';
import Modal from './Modal';

class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            searchText: '',
            movies: [],
            isModalOpen: false,
        };
    }

     handleSearch = async (e) => {

        this.setState({
          searchText: e.target.value,
          movies: [],
        });

        const searchInput = e.target.value;
        const url = "http://www.omdbapi.com/?apikey=4b601aab&s=*"+ searchInput +"*&type=movie&page=1";
        const response = await fetch(url);
        const responseObj = await response.json(); 
  
        if (responseObj.Response === 'True') {
            const movieList = responseObj.Search; 
            this.setState({
                movies: [...movieList],
              });
        }

      };

    handleModal = () => {
        this.setState((prevState) => {
            return {
              isModalOpen: !prevState.isModalOpen,
            };
          });
    }

    showMovieDetails = () => {
        this.handleModal();

    }

    render() {    
        const { searchText, movies, isModalOpen } = this.state;

        return (
            <>
                <Search searchText={searchText} changeFunc={this.handleSearch}/>
                <br/>
                {!!movies.length && <MovieList movieList={movies} modalFunc={this.handleModal}/>}
                {isModalOpen && <Modal handleModal={this.handleModal}/>}                                
            </>
        );
    }
}


export default App;
